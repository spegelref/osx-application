package se.zxlisys.osx.view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class UnifiedStatusBar extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final Color OSX_BORDER_TOP_COLOR = 
			new Color(0x72, 0x72, 0x72);

	private UnifiedWindowGrabber grabber;
	
	public UnifiedStatusBar() {
		
		grabber = new UnifiedWindowGrabber(this);
		
		addMouseListener(grabber);
		addMouseMotionListener(grabber);
	}
	
	@Override
	public Border getBorder() {
		return BorderFactory.createMatteBorder(1, 0, 0, 0,
				OSX_BORDER_TOP_COLOR);
	}
}
