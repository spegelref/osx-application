package se.zxlisys.osx.view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class UnifiedToolbarPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	private static final Color OSX_BORDER_BOTTOM_COLOR = 
			new Color(0xA0, 0xA0, 0xA0);

	private UnifiedWindowGrabber grabber;
	
	public UnifiedToolbarPanel() {
		setOpaque(false);
		setBorder(BorderFactory.createEmptyBorder());
		
		grabber = new UnifiedWindowGrabber(this);
		
		addMouseListener(grabber);
		addMouseMotionListener(grabber);
	}

	@Override
	public Border getBorder() {
		return BorderFactory.createMatteBorder(0, 0, 1, 0,
				OSX_BORDER_BOTTOM_COLOR);
	}
}
