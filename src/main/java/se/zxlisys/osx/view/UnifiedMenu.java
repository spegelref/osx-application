package se.zxlisys.osx.view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class UnifiedMenu extends JMenuBar {
	private static final long serialVersionUID = 1L;

	// File menu
	private JMenu file;

	private JMenuItem fileNewItem;
	private JMenuItem fileOpen;
	// --------
	private JMenuItem filePrint;

	
	// Window menu
	private JMenu window;

	private JMenuItem windowMinimize;
	private JMenuItem windowZoom;
	// --------
	private JMenuItem windowBringToFront;
	
	
	// Help menu
	private JMenu help;

	private JMenuItem helpManual;
	private JMenuItem helpWebsite;
	
	public UnifiedMenu() {
		initialize();
	}

	private void initialize() {
		// Instantiate file menu.
		file = new JMenu("File");
		fileNewItem = new JMenuItem("New item");
		fileOpen = new JMenuItem("Open");
		filePrint = new JMenuItem("Print");
		
		file.add(fileNewItem);
		file.add(fileOpen);
		file.add(new JSeparator());
		file.add(filePrint);
		
		// Instantiate window menu.
		window = new JMenu("Window");
		windowMinimize = new JMenuItem("Minimize");
		windowZoom = new JMenuItem("Zoom");
		windowBringToFront = new JMenuItem("Bring All to Front");

		window.add(windowMinimize);
		window.add(windowZoom);
		window.add(new JSeparator());
		window.add(windowBringToFront);
		
		// Instantiate help menu.
		help = new JMenu("Help");
		helpManual = new JMenuItem("Unified Manual");
		helpWebsite = new JMenuItem("Open website");
		
		help.add(helpManual);
		help.add(helpWebsite);

		
		// Add menus to the menu bar.
		add(file);
		add(window);
		add(help);
	}
}
