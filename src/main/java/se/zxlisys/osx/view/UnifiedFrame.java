package se.zxlisys.osx.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.lang.reflect.Method;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class UnifiedFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	
	private UnifiedToolbarPanel toolbar;
	
	private JPanel main;
	
	private UnifiedStatusBar status;
	
	public UnifiedFrame() {
		enableFullScreenMode(this);
		
		initialize();
	}
	
	private void initialize() {
		toolbar = new UnifiedToolbarPanel();
		toolbar.setPreferredSize(new Dimension(1, 50));
		
		toolbar.setLayout(new BoxLayout(toolbar, BoxLayout.LINE_AXIS));
		//toolbar.add(new JButton("Button 1"));
		//toolbar.add(new JButton("Button 2"));
		
		toolbar.add(Box.createHorizontalGlue());
		
		JTextField searchBox = new JTextField();
//		searchBox.putClientProperty("JTextField.variant", "search");
		searchBox.setSize(new Dimension(75, 10));
		//toolbar.add(searchBox);
		
		main = new JPanel();
		main.setBackground(new Color(0xED, 0xED, 0xED));
		
		status = new UnifiedStatusBar();
		status.setPreferredSize(new Dimension(1, 15));
		
		contentPane = new JPanel();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(toolbar, BorderLayout.NORTH);
		contentPane.add(main, BorderLayout.CENTER);
		contentPane.add(status, BorderLayout.SOUTH);
		
		// Apple specific code
		getRootPane().putClientProperty("apple.awt.brushMetalLook",
				Boolean.TRUE);
		
		setTitle("Unified Application");
		
		setJMenuBar(new UnifiedMenu());
		
		setMinimumSize(new Dimension(450, 300));
		setPreferredSize(new Dimension(450, 300));
		
		setContentPane(contentPane);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		pack();
	}
	
	public static void enableFullScreenMode(Window window) {
		String className = "com.apple.eawt.FullScreenUtilities";
        String methodName = "setWindowCanFullScreen";
		
	    try {
	        Class<?> util = Class.forName(className);
	        Method method = util.getMethod(methodName, new Class<?>[] {
	        		Window.class, boolean.class
	        });
	        
	        method.invoke(null, window, true);
	    } catch (Throwable e) {
	    	System.out.println("Full screen is not supported"); //TODO: Log
	    }
	}
}
