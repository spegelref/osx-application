package se.zxlisys.osx;

import se.zxlisys.osx.util.Platform;
import se.zxlisys.osx.view.UnifiedApplicationAdapter;
import se.zxlisys.osx.view.UnifiedFrame;

import com.apple.eawt.Application;

public class Program {
	public static void main(String[] args) {
		Platform.osx();
		
	    System.setProperty("apple.awt.graphics.EnableQ2DX", "true");
	    System.setProperty("apple.laf.useScreenMenuBar", "true");
	    System.setProperty("com.apple.mrj.application.apple.menu.about.name", 
	    		"Unified Swing");
	    
	    Application app = Application.getApplication();
	    app.addApplicationListener(new UnifiedApplicationAdapter());
	    
	    app.setEnabledAboutMenu(true);
	    app.setEnabledPreferencesMenu(true);
	    
		UnifiedFrame toolbar = new UnifiedFrame();
		toolbar.setVisible(true);
	}
}
