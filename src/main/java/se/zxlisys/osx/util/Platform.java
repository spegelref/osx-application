package se.zxlisys.osx.util;

public class Platform {
	public static boolean linux() {
		return platformCheck("linux");
	}
	
	public static boolean osx() {
		return platformCheck("mac os x");
	}

	public static boolean windows() {
		return platformCheck("windows");
	}
	
	private static boolean platformCheck(String platf) {		
			String os = System.getProperty("os.name").toLowerCase();
			return os.startsWith(platf);
	}
}
