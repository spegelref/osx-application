package se.zxlisys.osx.view;

import java.awt.Point;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

public class UnifiedWindowGrabber implements MouseListener, 
		MouseMotionListener {

	private JComponent owner;
	private Point start;
	
	public UnifiedWindowGrabber(JComponent owner) {
		this.owner = owner;
	}
	
	// MouseListener implementation

	@Override
	public void mousePressed(MouseEvent e) {
		start = e.getPoint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		start = null;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	// MouseMotionListener

	@Override
	public void mouseDragged(MouseEvent e) {
		Window window = SwingUtilities.getWindowAncestor(owner);

		if (window != null && e.getButton() == MouseEvent.BUTTON1) {
			Point pframe = window.getLocation();
			Point pcur = e.getPoint();

			window.setLocation(pframe.x + (pcur.x - start.x), pframe.y
					+ (pcur.y - start.y));
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}
}
