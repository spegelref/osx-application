package se.zxlisys.osx.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

public class UnifiedAboutDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private JButton button;
	
	public UnifiedAboutDialog() {
		initialize();
	}
	
	private void initialize() {
		setLayout(new FlowLayout());
		
		button = new JButton("Close");
		button.addActionListener(this);
		
		add(button);
		
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			setVisible(false);
			dispose();
		}
	}
}
