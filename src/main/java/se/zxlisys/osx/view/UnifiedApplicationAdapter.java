package se.zxlisys.osx.view;

import javax.swing.WindowConstants;

import com.apple.eawt.ApplicationAdapter;
import com.apple.eawt.ApplicationEvent;

public class UnifiedApplicationAdapter extends ApplicationAdapter {
	public UnifiedApplicationAdapter() {
		
	}
	
	@Override
	public void handleAbout(ApplicationEvent e) {
	    e.setHandled(true);
	    
	    UnifiedAboutDialog dialog = new UnifiedAboutDialog();
	    dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	    dialog.setVisible(true);
	}
	
	@Override
	public void handlePreferences(ApplicationEvent e) {
		super.handlePreferences(e);
		System.out.println("Preferences!");
	}
	
	@Override
	public void handleQuit(ApplicationEvent e) {
		System.exit(0);
	}
}
